import requests


def test_registration_status_code_positive():
    response = requests.get('http://localhost:8000/register')
    assert response.status_code == 200


def test_registration_post_positive():

    data = {
        'username': 'admin2@gmail.com',
        'password': 'admin212345'
    }
    response = requests.post('http://localhost:8000/register', data=data)
    assert response.status_code == 201


def test_registration_duplicate_user():
    data = {
        'username': 'admin2@gmail.com',
        'password': 'admin212345'
    }
    response = requests.post('http://localhost:8000/register', data=data)
    assert response.status_code == 400


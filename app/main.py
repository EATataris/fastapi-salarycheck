import random
from typing import Optional, Tuple

from fastapi import FastAPI, HTTPException, Request, Depends
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session
from database import engine, get_db
from models.user import User, Salary
from models.schemas import UserCreate
from database import Base
from datetime import date, timedelta
from pydantic import ValidationError
from api.auth import login_for_access_token, get_current_user_from_token


Base.metadata.create_all(bind=engine)

app = FastAPI()
templates = Jinja2Templates(directory='templates')


@app.get("/", response_class=HTMLResponse)
def main(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


@app.get("/register", response_class=HTMLResponse)
async def register(request: Request):
    return templates.TemplateResponse("register.html", {"request": request})


@app.post("/register")
async def register_user(request: Request, db: Session = Depends(get_db)):
    try:
        user_form = await request.form()
        user = UserCreate(username=user_form.get("username"), password=user_form.get("password"))
    except ValidationError as e:
        raise HTTPException(status_code=400, detail=str(e))

    db_user = db.query(User).filter_by(username=user.username).first()

    if db_user:
        raise HTTPException(status_code=400, detail="Username already registered")

    new_user = User(username=user.username, password=user.password)
    db.add(new_user)
    db.commit()

    new_user_salary = Salary(salary=generate_salary(), next_promotion=generate_next_promotion(), user=new_user)
    db.add(new_user_salary)
    db.commit()

    return templates.TemplateResponse("index.html", {"request": request}, status_code=201)


def generate_salary():
    return random.randint(30000, 100000)


def generate_next_promotion():
    return date.today() + timedelta(days=random.randint(0, 365))


def get_authorization_scheme_param(
    authorization_header_value: Optional[str],
) -> Tuple[str, str]:
    if not authorization_header_value:
        return "", ""
    scheme, _, param = authorization_header_value.partition(" ")
    return scheme, param


@app.post('/')
async def login(request: Request, db: Session = Depends(get_db)):
    login_form = await request.form()
    username = login_form.get('username')
    password = login_form.get('password')

    if not username or not password:
        return templates.TemplateResponse(
            'index.html',
            {
                'request': request,
                'error': "Invalid username or password"
            }
        )
    response = RedirectResponse(url='/salary', status_code=303)
    login_for_access_token(response, username=username, password=password, db=db)

    return response


@app.get('/salary')
async def get_salary(request: Request, db: Session = Depends(get_db)):

    token = request.cookies.get("access_token")

    scheme, param = get_authorization_scheme_param(
        token
    )
    user = get_current_user_from_token(param, db)

    salary = db.query(Salary).filter(Salary.user_id == user.id).first()

    return templates.TemplateResponse("salary.html", {"request": request, 'salary': salary})


if __name__ == "__main__":
    import uvicorn
    uvicorn.run("main:app", host="localhost", port=8000, reload=True)
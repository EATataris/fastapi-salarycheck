from pydantic import BaseModel


class UserCreate(BaseModel):
    username: str
    password: str


class LoginData(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    username: str
    password: str
from sqlalchemy import Column, Integer, String, Float, Date, ForeignKey
from database import Base
from sqlalchemy.orm import relationship


class Salary(Base):
    __tablename__ = 'salaries'

    id = Column(Integer, primary_key=True, index=True)
    salary = Column(Integer)
    next_promotion = Column(Date)
    user_id = Column(Integer, ForeignKey('users.id'))

    user = relationship('User', back_populates='salary')


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    password = Column(String, unique=True)

    salary = relationship('Salary', uselist=False, back_populates='user')






